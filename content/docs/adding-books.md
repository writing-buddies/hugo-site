+++
title = "Adding Books"
weight = 4
date = "2020-11-21"
+++

If you want to add a new book to the site, you will need to PM one of the admins in the Habitica guild, you will need to provide book title, author's name, cover, and a short description of the book.

For the cover, it needs to be a file. You can send a link to the page where the file is or insert the file itself. If you don't have a cover, you can look on sites like [Unsplash](https://unsplash.com) or [Pixabay](https://pixabay.com) for one. Just send the URL of the picture you chose in the email.

One of the admins will try to get back to you as soon as possible and let you know when you can start adding your chapters. After you receive a confirmation from an admin, you should see a folder with the name of your book. Inside your book title's folder is where you will put chapters for your book. There is also a file called "_index.md", **BE CAREFUL** when you touch this file, it holds the metadata for your book.

## Adding Chapters
Again, this is pretty straightforward. The only thing you need to worry about is the "Chapter" label. This needs to be a number and it just lets the site know the chapter orders, no biggie. There isn't a limit to have many chapters it can have, create a hundred if you want (why would you do that though?). Just, try not to combine books, that doesn't make sense. It also doesn't matter what you name the chapters, "Chapter 1" is just as fine as "Chapter 1, The Beginning Of The End."
