+++
title = "Kidnapped and Afraid"
subtitle = ""
author = "Bravisha Skietano"
date = "2020-06-25"
weight = 5
bookCover = "bookCovers/kidnapped-and-afraid.jpg"
+++

Image by <a href="https://pixabay.com/users/darksouls1-2189876/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=4182841">Enrique Meseguer</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=4182841">Pixabay</a>
