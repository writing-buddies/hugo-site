+++
title = "Welcome!"
weight = 1
date = "2020-11-21"
+++

First, thanks so much for considering contributing to this site and keeping it alive. I know it's not very fancy (yet) or popular, but your contributions really mean a lot to me. Who am I? Oh, you can call me Bravisha Skietano. I am the site's creator and the head adminstrator here.

First off, this site is completely free to use. There are rules to follow but you will **NEVER** have to pay a thing as long as I can say something about it. I created this site as a safe place for others to share their stories without worrying about fees or privacy thiefs.

The site is hosted on GitLab and Netlify with Netlify CMS as the content manager. That means I paid nothing to get this site up and running and you shouldn't either.

Again, thanks so much for contributing. If you have any questions, don't be afraid to post them in the comments below or email them to writing-buddies@outlook.com.

Happy writing!