+++
title = "Rules & Guidelines"
weight = 2
date = "2020-11-21"
+++

Rules, rules, rules. I know, they're not always fun but they do help keep order (usually). This rules try to keep everything nice and fair for everyone, as well as help me keep my sanity.

1. **Keep profanity to an absolute minimum.** Not every likes to read a book with cuss words in every paragraph.
2. **No explicit content.** In case you need a more detailed explanation, that means no sex scenes or detailed conversations about them. Kissing is okay, making out isn't. I don't care how much your characters love each other, what they do in private isn't our business.
3. **Be respectful of others gender, social status, religion, and convictions.** We are all entitled to our own opinions, don't bash on someone just because they don't share yours.
4. **No alcohol, drugs, or encouragement of them.** Do I need to say more?
5. **No graphic violence.** Who doesn't like an awesome fight scene? I definately do; but, I don't like those scenes where the violence is so graphic you can picture it exactly and it ain't pretty. Makes you want to puke. Don't do that to your readers.
5. **Try to keep homosexuality out of this as much as possible.** First off, I don't have hate the LGBTQ+ community, I mean it. It's just that with everything going on and my own personal convictions, I would like it if you don't try to include a bunch of homosexual relationships in your stories. If one of your characters is gay, that's nice, I don't mind; however, if your book centers around a gay person dating another gay, please don't share it here. It was hard for me to make this decision and even harder to stand by it so bash on me for it. There are many other sites out there that will take those stories, go to them.

Those are not so bad, right? Now, regarding the last rule... I get it, okay. I know people who are homosexual and I know others who have friends who are homosexual. Unfortunately, I... I don't want to be a part of it. Like I said, I'm not banning stories with homosexuals, I'm just banning the ones that focus primarily on them. Don't hate on me, okay? Whenever I see sites that have those stories, along with sex stories, they just take over and every story ends up having something about homosexuals and how you're stupid for not liking them. I just don't want that to happen here, give me a break.
