+++
title = "Contacting An Admin"
weight = "5"
date = "2020-11-21"
+++

If you see anything that goes against the rules or have a question, don't hesitate to contact the adminstrators. That's what we're there for after all.

* [GitLab](https://gitlab.com/writing-buddies)
* [Habitca guild](https://habitica.com/groups/guild/0b3b0faf-41f0-48e7-8e0f-5e2793658041)

## Adminstrators
* Bravisha_Skietano
