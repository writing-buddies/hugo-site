+++
title = "Using the CMS"
weight = 3
date = "2020-06-25"
+++

CMS stands for Content Management System, which is what you would use to write posts for a site. Think of it as a user-friendly interface like the one WordPress or Wattpad uses.

To access the CMS for this site, all you need to do is go to https://writing-buddies.netlify.app/admin. When you access this page, you should see a button saying "Login with Netlify Identity." Click that and you will be asked to log in or sign up.

## Signing Up
When you sign up for this site, you will be asked for your name, email, and password or you can sign up using Google, GitHub, GitLab, or BitBucket. Your name should be the name you want to be recognized by on the site, whether that is your real name or a pen name is up to you.

## Adding Content

> **DO NOT** touch content that does not belong to you **unless** you have been given permission by the original author. If you do this, you will be notified by the admin and receive a penalty. If you receive three penalties, you will be removed from the site.

On the left, you will see a list of folders. Each of these folders applies to the type of content and/or book they hold. The first three folders (Poetry, Plays, and Flash Fiction) are self-explanatory. You put your poems, plays, and flash fiction in there.

> Flash fiction is bite-sized, one-page stories. Think, short stories with only one chapter.

Following those folders are the folders for specific books. Inside these folders are the chapters for that specific book. At the current moment, you will not be able to create folders. If you want to add a book, read on.

To add posts to a specific folder, click on that folder and click on the button "New Post". Fill in the boxes on your left and save your work by clicking the "Save" button on the top left. On the right, you will see a basic preview of your post.

Set the status of your post (Draft, In review, or Ready) by using the "Set status" dropdown menu. When you're ready to publish the post, set the status of your post to "Ready" and click publish.

> Please make sure your posts follow the guidelines set in the rules chapter!

Oh, almost forgot! If you are writing poems, you might need to add a backword-leaning slash (\)) where you want a line-break. This has to do with Markdown and I'm not sure if the rich-text editor will recognize line breaks. So, if you notice your poems don't have line breaks, go back and add some slashes
